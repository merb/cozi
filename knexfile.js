const appConfig = require('../config/default')
const {
  knex
} = appConfig.dbOptions

module.exports = {
  client: 'mysql2',
  connection: `${knex.protocol}://${knex.user}:${knex.password}@${knex.host}:${knex.port}/${knex.database}`
}
