import 'primevue/resources/themes/saga-blue/theme.css'
import 'primevue/resources/primevue.css'
import 'primeicons/primeicons.css'

import '~/sass/styles.scss'
import 'primeflex/primeflex.css'
import { browserDetect } from '~/js/browser-detect'
import Vue from 'vue'
import VueMeta from 'vue-meta'
import Vue2Filters from 'vue2-filters'
import vueScrollTo from 'vue-scrollto'
import { Router } from '~/vue/router'
import appConfig from '~/vue/root.vue'

import { axios } from '~/js/axios'
import { noty } from '~/js/noty'
import { LocalDb } from '~/js/localDb'

// -------------------
// utilities
// -------------------
browserDetect()

// -------------------
// plug-ins
// -------------------
Vue.prototype.$primevue = { ripple: true }
Vue.prototype.$axios = axios
Vue.prototype.$noty = noty

export const VueScrollTo = vueScrollTo
Vue.use(vueScrollTo)

export const localDb = LocalDb('app', 'v1')
Vue.prototype.$localDb = localDb

// router
Vue.use(Router)
Vue.use(VueMeta)

// -------------------
// Filters
// -------------------
// Docs --> https://github.com/freearhey/vue2-filters
Vue.use(Vue2Filters)

Vue.filter('default', (value, defaultValue) => {
  if (value) return value
  else return defaultValue
})

// -------------------
// Root
// -------------------
export const eventBus = new Vue({
  data: {
    // default values
  }
})

export const app = new Vue(appConfig)
