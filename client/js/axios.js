import Axios from 'axios'
import { noty } from '~/js/noty'
import { eventBus, localDb } from '~'

Axios.interceptors.response.use(res => {
  // Process all success responses
  if (process.env.NODE_ENV === 'development') console.log('Hey look a success response: ', res)
  return res
}, err => {
  // Process all error responses
  const error = err.response
  if (error && process.env.NODE_ENV === 'development') {
    // The request was made and the server
    // responded with a status code that
    // falls out of the range of 2xx
    console.error('Err - Headers: ', error.headers)
    console.error('Err - Status: ', error.status)
    console.error('Err - Data: ', error.data)
  }

  if (error.status === 403) {
    noty({
      type: 'error',
      text: 'You are not authorized for that'
    })
  }

  if (error.status === 401) {
    noty({
      type: 'error',
      text: 'Need to log back in...'
    })

    clearAuth()
  }

  return Promise.reject(error)
})

export function addAuth (token) {
  Axios.defaults.headers.common.Authorization = `Bearer ${token}`
}

export function clearAuth () {
  localDb.set({
    token: ''
  }).then(() => {
    Axios.defaults.headers.common.Authorization = ''
    eventBus.$emit('needToLogBackIn')
  })
}

export const axios = Axios
