import Noty from 'noty'

export function noty ({
  type = 'info', // alert, success, error, warning, info
  text = 'This is the default message',
  timeout = 2000,
  progressBar = true
}) {
  return new Noty({
    layout: 'topRight',
    theme: 'barn',
    closeWith: ['click'],
    animation: {
      open: 'noty_effects_open',
      close: 'noty_effects_close'
    },
    visibilityControl: true,
    type,
    text,
    timeout,
    progressBar
  }).show()
}
