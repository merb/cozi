export function LocalDb (name, prefix) {
  return {
    name: `${prefix}-${name}`,
    cache: undefined,
    get (propKey) {
      return new Promise((resolve, reject) => {
        if (this.cache) {
          if (propKey.length) return resolve(this.cache[propKey])
          else return resolve(this.cache)
        } else {
          const got = JSON.parse(localStorage.getItem(this.name))
          if (got) {
            if (propKey.length) resolve(got[propKey])
            else resolve(got)
          } else resolve(null)
        }
      })
    },
    set (prop) {
      return new Promise((resolve, reject) => {
        if (this.cache) {
          Object.assign(this.cache, prop)
        } else {
          const got = JSON.parse(localStorage.getItem(this.name))

          if (got) {
            this.cache = got
            Object.assign(this.cache, prop)
          } else {
            this.cache = prop
          }
        }

        localStorage.setItem(this.name, JSON.stringify(this.cache))
        return resolve()
      })
    }
  }
}
