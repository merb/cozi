import { getEl } from '~/js/utility'

export const browserDetect = () => {
  if (/Trident\/|MSIE /.test(window.navigator.userAgent)) {
    const body = getEl('body')
    body.classList.add('ie')
  }

  if (/Edge/.test(window.navigator.userAgent)) {
    const body = getEl('body')
    body.classList.add('edge')
  }
}
