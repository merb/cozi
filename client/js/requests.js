import { axios, addAuth } from '~/js/axios'
import { localDb } from '~'
// import { noty } from '~/js/noty'

export async function login (email, password) {
  const res = await axios.post('/api/users/authorize', { email, password })
  await localDb.set({
    token: res.data.token,
    user: res.data.user || {}
  })
  addAuth(res.data.token)

  return res
}
