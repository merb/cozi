import Router from 'vue-router'
import { localDb } from '~'

// Pages
import Home from '~/vue/pages/Home.vue'
import Login from '~/vue/pages/Login.vue'
import App from '~/vue/pages/App.vue'
import Profile from '~/vue/pages/Profile.vue'

// import Settings from '~/vue/pages/Settings.vue'
import Custom404 from '~/vue/pages/Custom404.vue'

// Partials
import PublicNav from '~/vue/components/partials/public-nav.vue'
import SecureNav from '~/vue/components/partials/secure-nav.vue'

function isProtected (to, from, next) {
  localDb.get('token')
    .then(token => {
      if (!token) next('/login')
      else next()
    })
}

const router = new Router({
  mode: 'history',
  base: '/',
  routes: [
    {
      path: '/',
      name: 'home',
      components: {
        default: Home,
        nav: PublicNav
      }
    },
    {
      path: '/login',
      name: 'login',
      components: {
        default: Login,
        nav: PublicNav
      }
    },
    {
      path: '/app',
      name: 'app',
      components: {
        default: App,
        nav: SecureNav
      },
      children: [
        {
          path: 'profile',
          name: 'profile',
          component: Profile,
          beforeEnter: isProtected
        }
      ],
      beforeEnter: isProtected
    },

    // {
    //   path: '/settings',
    //   name: 'settings',
    //   component: Settings
    // },

    // 404 template
    {
      path: '*',
      component: Custom404
    }
  ]
})

export { router, Router }
