const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin')
const TerserPlugin = require('terser-webpack-plugin')
const VueLoaderPlugin = require('vue-loader/lib/plugin')
const webpack = require('webpack')

module.exports = (env, argv) => {
  console.log('env: ', env)
  console.log('argv: ', argv)

  const config = {
    name: 'main-config',
    entry: {
      main: './index.js'
    },
    output: {
      filename: '[name].js',
      path: path.resolve(__dirname, '../public/assets')
    },
    mode: argv.mode || 'development', // production|development
    watch: false,
    resolve: {
      alias: {
        '~': path.resolve(__dirname, './')
      },
      modules: ['node_modules']
    },
    devtool: 'inline-source-map',
    module: {
      rules: [
        {
          test: /\.scss$/,
          use: [
            MiniCssExtractPlugin.loader,
            // 'style-loader',
            'css-loader',
            {
              loader: 'sass-loader',
              options: {
                additionalData: '@import "~/sass/vars";'
              }
            }
          ],
          sideEffects: true
        },
        {
          test: /\.css$/,
          use: [
            MiniCssExtractPlugin.loader,
            'css-loader'
          ],
          sideEffects: true
        },
        {
          test: /\.pcss$/,
          use: [
            MiniCssExtractPlugin.loader,
            'css-loader',
            'postcss-loader',
            'sass-loader'
          ],
          sideEffects: true
        },
        {
          test: /\.js$/,
          exclude: /(node_modules|bower_components)/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: [
                [
                  '@babel/preset-env',
                  {
                    debug: false,
                    useBuiltIns: 'usage',
                    corejs: 3,
                    targets: {
                      browsers: [
                        'last 2 versions',
                        'IE 11',
                        'safari 8'
                      ]
                    }
                  }
                ]
              ],
              plugins: [
                '@babel/plugin-proposal-class-properties',
                '@babel/plugin-transform-runtime',
                '@babel/plugin-transform-object-assign'
              ]
            }
          },
          sideEffects: false
        },
        {
          test: /\.(png|svg|jpg|gif|ico)$/,
          // exclude: /(node_modules|bower_components)/,
          use: {
            loader: 'file-loader',
            options: {
              name: 'imgs/[name].[ext]'
            }
          },
          sideEffects: true
        },
        {
          test: /\.(woff|woff2|eot|ttf|otf)$/,
          // exclude: /(node_modules|bower_components)/,
          use: {
            loader: 'file-loader',
            options: {
              name: 'fonts/[name].[ext]'
            }
          },
          sideEffects: true
        },
        {
          test: /\.json$/,
          loader: 'json-loader',
          sideEffects: true
        },
        {
          test: /\.(pdf|xlsx|docx|pptx|mp4|xml)$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: 'files/[name].[ext]'
              }
            }
          ],
          sideEffects: true
        },
        {
          test: /\.vue$/,
          loader: 'vue-loader',
          sideEffects: false
        }
      ]
    },
    optimization: {
      minimize: argv.mode === 'production',
      minimizer: [
        new TerserPlugin({
          test: /\.js($|\?)/i,
          parallel: 4,
          sourceMap: false,
          extractComments: false,
          terserOptions: {
            compress: {
              drop_console: true,
              conditionals: true,
              unused: true,
              comparisons: true,
              dead_code: true,
              if_return: true,
              join_vars: true
            },
            output: {
              comments: false
            }
          }
        })
      ],
      removeEmptyChunks: true,
      namedModules: true,
      namedChunks: true
      // splitChunks: {
      //   cacheGroups: {
      //     commons: {
      //       name: 'commons',
      //       test: /\.js$/,
      //       chunks: 'initial',
      //       // maxSize: 2
      //     },
      //     styles: {
      //       name: 'styles',
      //       test: /\.css$|\.pcss$|\.scss$/,
      //       chunks: 'initial',
      //       enforce: true,
      //     }
      //   }
      // }
    },
    plugins: [
      new MiniCssExtractPlugin({
        // filename: 'css' // Use this if spliting chunks as it adds 'styles' to the name
        filename: 'styles.css'
      }),
      new OptimizeCssAssetsPlugin({
        assetNameRegExp: /\.css$/g,
        cssProcessor: require('cssnano'),
        cssProcessorOptions: { discardComments: { removeAll: true } },
        canPrint: true
      }),
      new VueLoaderPlugin(),
      new webpack.EnvironmentPlugin({
        NODE_ENV: argv.mode || 'development'
      })
    ]
  }

  return config
}
