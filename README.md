# Cozi
An opinionated Koa + Objectionjs starter

## Features
- Koa server
- Mysql support with objectionjs
- API validation with joi
- URL parameter/query based filtering with objection-filter
- Auto reload dev env using supervisor
- helmet
- dotEnv + centeralized config frozen global + env helpers
- Logs with Pino
- Users auth (JWT for now) and password hashing (argon2)

## ToDo
- Add Response sanitation
- Add RBAC
- Add knex auto migrations
- Add html templating (nunjucks most likely)
- Add cli for auto creating models and routes
- Add swagger
- Add more auth options

## Code Style
Using [StandardJS](https://standardjs.com/)
