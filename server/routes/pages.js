'use strict'

const Router = require('@koa/router')
const router = new Router()

router.get('/(.*)', async (ctx, next) => {
  await ctx.render('home', {
    data: 'wow look at this!'
  })
})

module.exports = router
