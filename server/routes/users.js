'use strict'

const Router = require('@koa/router')
const router = new Router()
router.prefix('/api')
const koaBody = require('koa-body')
const findQuery = require('objection-find')
const { User } = cozi.db.models

const joi = require('joi')
const validate = require('../middleware/joiValidator')
const jwtProtect = require('../middleware/jwtProtect')

const argon2 = require('argon2')
const jwt = require('jsonwebtoken')

// Get users
router.get('/users', jwtProtect, async (ctx) => {
  const users = await findQuery(User)
    .allow(['id', 'userName'])
    .allowEager([])
    .build(ctx.query)

  const cleaned = await User.sanitize(users, ctx.state.user)
  ctx.body = cleaned
})

// Create user
router.post('/users',
  koaBody(),
  validate(
    joi.object({
      userName: joi.string().required(),
      email: joi.string().email().required(),
      password: joi.string().required()
    })
  ),
  async ctx => {
    const users = await User.query().insertAndFetch(ctx.request.body)
    if (users.length) {
      const cleaned = await User.sanitize(users[0], ctx.state.user)
      ctx.body = cleaned
    } else ctx.body = users
  }
)

// Authorize a user and give token
router.post('/users/authorize',
  koaBody(),
  validate(
    joi.object({
      email: joi.string().email().required(),
      password: joi.string().required()
    })
  ),
  async (ctx) => {
    console.log('/users/authorize body: ', ctx.request.body)

    const {
      email,
      password
    } = ctx.request.body
    const foundUser = await User.query().findOne({
      email
    })

    if (foundUser) {
      // ctx.body = user
      const isMatched = await argon2.verify(foundUser.password, password)
      if (isMatched) {
        // Generate and return jwt token
        const token = jwt.sign({
          id: foundUser.id
        },
        cozi.config.auth.secret,
        cozi.config.auth.options)

        const user = await User.sanitize(foundUser, foundUser)

        ctx.body = {
          token: token,
          user
        }
      } else {
        loginFailed()
      }
    } else {
      loginFailed()
    }

    function loginFailed () {
      ctx.status = 401
      ctx.body = {
        status: ctx.status,
        message: 'Username or password is incorrect'
      }
    }
  }
)

module.exports = router
