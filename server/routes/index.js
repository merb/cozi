'use strict'

const allowedRoutes = [
  require('./users'),
  require('./pages')
]

module.exports = allowedRoutes
