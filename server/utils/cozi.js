'use strict'

// Project global
function init (obj) {
  const cozi = obj
  global.cozi = cozi

  Object.freeze(global.cozi)
}

module.exports = init
