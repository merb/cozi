'use strict'

async function dbSetup (config) {
  const {
    dbOptions
  } = config

  // Init Knex
  const Knex = require('knex')
  const knex = Knex(dbOptions.knex)

  const { Model } = require('objection')
  // Bind all Models to the knex instance. You only
  // need to do this once before you use any of
  // your model classes.
  Model.knex(knex)

  // Model library
  const models = {}

  const packagedModels = require('../models')
  for (const model of packagedModels) {
    const unwrapped = await model()
    models[unwrapped.modelName] = unwrapped.model
  }

  return {
    models,
    knex
  }
}

module.exports = dbSetup
