'use strict'

const jwt = require('jsonwebtoken')
const { User } = cozi.db.models

module.exports = async (ctx, next) => {
  const parts = ctx.header.authorization.trim().split(' ')

  if (parts.length === 2) {
    const scheme = parts[0]
    const token = parts[1]

    if (/^Bearer$/i.test(scheme)) {
      try {
        const payload = jwt.verify(token, cozi.config.auth.secret)

        if (payload.id) {
          try {
            const user = await User.query().findById(payload.id)
            ctx.state.user = user
            await next()
          } catch (error) {
            ctx.throw(401, 'Not Authorized')
          }
        }
      } catch (error) {
        console.error('jwt error: ', error)
        ctx.throw(401, error)
      }
    } else ctx.throw(401, 'Bad Authorization header format. Format is "Authorization: Bearer <token>"')
  }
}
