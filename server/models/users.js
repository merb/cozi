'use strict'

const { Model } = require('objection')
const modelName = 'User'
const tableName = 'users'
const dateFormat = require('date-fns/format')
const argon2 = require('argon2')

class NewModel extends Model {
  // Table name is the only required property.
  static get tableName () {
    return tableName
  }

  static get jsonSchema () {
    return {
      type: 'object',
      required: ['email', 'password'],

      properties: {
        userName: { type: 'string' },
        email: { type: 'string' },
        password: { type: 'string' }
      }
    }
  }

  static async sanitize (data, curUser) {
    console.log('curUser: ', curUser)
    if (data === null || data === undefined) return data

    if (Array.isArray(data)) {
      for (const item of data) {
        if (!curUser || curUser.id !== item.id) delete item.email
        delete item.password
      }
    } else if (typeof data === 'object') {
      if (!curUser || curUser.id !== data.id) delete data.email
      delete data.password
    }

    return data
  }

  // =-=-=-=-=-=-=-=-=-=-=-=
  // Lifecycle Callbacks
  // https://vincit.github.io/objection.js/api/model/instance-methods.html#beforeinsert
  // =-=-=-=-=-=-=-=-=-=-=-=

  async $beforeInsert (queryContext) {
    this.created_at = dateFormat(new Date(), 'yyyy-MM-dd HH:mm:ss')

    const hash = await argon2.hash(this.password)
    this.password = hash
  }

  // async $afterInsert (queryContext) {
  //   await super.$afterInsert(queryContext)
  // }

  $beforeUpdate (opt, queryContext) {
    this.updated_at = dateFormat(new Date(), 'yyyy-MM-dd HH:mm:ss')
  }

  // async $afterUpdate (opt, queryContext) {
  //   await super.$afterUpdate(queryContext)
  // }

  // $afterFind (queryContext) {}

  // These methods are only called for instance deletes started with $query() method.All hooks are instance methods.For deletes there is no instance for which to call the hook, except when $query() is used.Objection doesn't fetch the item just to call the hook for it to ensure predictable performance and prevent a whole class of concurrency bugs.
  // async $beforeDelete (queryContext) {
  //   await super.$beforeDelete(queryContext)
  // }

  // async $afterDelete (queryContext) {
  //   await super.$afterDelete(queryContext)
  // }
}

module.exports = async () => {
  const knex = Model.knex()
  const hasTable = await knex.schema.hasTable(tableName)
  if (!hasTable) {
    await knex.schema.createTable(tableName, table => {
      table.increments('id').primary()
      table.string('userName').unique()
      table.string('email').unique()
      table.string('password')
      table.timestamps()
    })
  }

  return {
    modelName,
    model: NewModel
  }
}
