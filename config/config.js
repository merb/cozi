'use strict'
// Get env variables
require('dotenv').config({
  path: '.env'
})

const path = require('path')
const env = require('../server/env-helper')

const config = {
  env: env('NODE_ENV', 'development'),

  server: {
    host: 'localhost',
    port: env.int('PORT', 3030),
    public: path.join(__dirname, '../public/'),
    trustProxy: true,
    logger: true
  },

  cors: {
    origin: env.array('CORS_ORIGIN', `http://localhost:${env.int('PORT', 3030)}`)
  },

  // AUTHENTICATION
  auth: {
    secret: env('JWT_SECRET', 'WoUlD^t=yOuL1K3_2kn0w#'),
    options: {
      algorithm: 'HS256',
      expiresIn: '1d'
    }
  },

  // DATABASE
  dbOptions: {

    // Options to pass to knex
    knex: {
      client: 'mysql2',
      connection: {
        host: env('DB_HOST', 'localhost'),
        port: env.int('DB_PORT', 3306),
        user: env('DB_USER'),
        password: env('DB_PASSWORD'),
        database: env('DB_NAME')
      }
    }
  }
}

module.exports = config
