'use strict'

const path = require('path')
const config = require('./config/config')
const chalk = require('chalk')
const Cozi = require('./server/utils/cozi')
const Koa = require('koa')
const jsonError = require('koa-json-error')
const dbConnect = require('./server/utils/dbConnect')
const koaLogger = require('koa-pino-logger')
const koaStatic = require('koa-static')
const helmet = require('koa-helmet')
const nunjucks = require('koa-nunjucks-async')
const veiwsPath = path.join(__dirname, './views')
const templateOptions = require('./server/utils/templateOptions')

async function initServer () {
  const db = await dbConnect(config).catch(err => {
    console.error(chalk.bgRed(' -=DB CONNECT FAILED=- '))
    throw new Error(err)
  })

  // Create global "cozi" app tools
  Cozi({
    config,
    db
  })

  const app = new Koa()
  // Add qs support
  // require('koa-qs')(app)

  // settings
  app.env = config.env
  app.proxy = config.server.trustProxy
  app.context.config = config

  // Communuty middleware
  // app.use(koaLogger({
  //   prettyPrint: config.env === 'development'
  // }))
  app.use(jsonError(err => {
    return {
      status: err.status,
      message: err.message,
      details: err.details
    }
  }))
  app.use(helmet())
  app.use(koaStatic(config.server.public))
  app.use(nunjucks(veiwsPath, templateOptions))

  // Routes
  const routers = require('./server/routes')
  for (const router of routers) {
    app.use(router.routes())
    app.use(router.allowedMethods())
  }

  return app
}

initServer()
  .then(app => {
    const server = app.listen(config.server.port)
    console.log(chalk.cyan('------------------------'))
    console.log(chalk.cyan('Server is up!'))
    console.log(chalk.cyan(`Enviorment: ${config.env}`))
    console.log(chalk.cyan(`Listening on: http://localhost:${server.address().port}`))
    console.log(chalk.cyan('------------------------'))
  })
  .catch(err => {
    console.error(chalk.bgRed(' -=Server Start up FAILED=- '))
    console.error(chalk.redBright(err))
    throw new Error(err)
  })

process.on('unhandledRejection', (reason, p) =>
  console.error('Unhandled Rejection at: Promise ', p, reason)
)
